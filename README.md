# Concept

I want to contain some statisctics about application and show it on merge request comments.  
How setup it:
1. Describe markdown template for statisctics
1. Set up pipeline jobs and artifacts for calculate statistics
1. Configure store for statistics

How implement it:  
1. Add tool for interaction with gitlab api for add merge request comments
1. Thinking how to store statisctics (store it as artifacts? or connect external database?)
1. Tool for put statisctics to markdown template (maybe use handlebars?)

Algorithm:
1. Find last merge request for this branch, if merge request does't exist exit.  
1. Get branch pipeline for target branch of merge request.  
1. Get stats of target branch (download artifacts).  
1. Get stats of current commit.  
1. Calculate stats diff and compile report template.  
1. Add note to MR or update MR description.  
